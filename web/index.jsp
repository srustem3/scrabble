<html>
<head>
    <title>Игра "Эрудит"</title>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <script type="text/javascript" src="index.js"></script>
</head>
<body>
<style type="text/css">
    .stable {font-size:15px;color:#333333;width:30%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
    .stable th {font-size:15px;background-color:#acc8cc;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:left;}
    .stable tr {background-color:#d4e3e5;}
    .stable td {font-size:15px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;}
    .stable tr:hover {background-color:#ffffff;}
</style>
    <form method="post" name="enterGame" action="EnterGame">
        Введите имя: <input type="text" id="username" value="Вася" ><br>
        Введите номер игры: <input type="number" min="0" max="99" id="gameId" value="0"><br>
        <input id="bEnterGame" type="button" value="Войти в игру" onclick="ClickEnterGame();">
    </form>
    <a href="/rules.html" target="_blank">Правила игры</a><br><br>
    <div id="gamelist"></div>
</body>
</html>
