<%@ page import="StaticClasses.General" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script>var audio = new Audio(); // Создаём новый элемент Audio
    audio.src = 'assets/finish.wav'; // Указываем путь к звуку "клика"
    audio.autoplay = true; // Автоматически запускаем</script>
    <title>Игра закончена. Большое спасибо за активное участие!</title>
    <style type="text/css">
    .stable {font-size:15px;color:#333333;width:33%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
    .stable th {font-size:15px;background-color:#acc8cc;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:center;}
    .stable tr {background-color:#d4e3e5;}
    .stable td {font-size:15px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:center;}
    .stable td:hover {background-color:#ffffff;}
    </style>
</head>
<body>
<h1>Результаты игры:</h1>
<%Cookie[] cookies = request.getCookies();
    int gameId = 0;
    if (cookies != null) {
        for (int i = 0; i < cookies.length; i++) {
            Cookie c = cookies[i];
            if (c.getName().equals("gameId")) {
                gameId = Integer.parseInt(c.getValue());
            }
        }
    } %>
<%=General.getGame(gameId).userList()%>
<br>
<a href="/">Вернуться на главную</a>
</body>
</html>
