// событие вызывается после полной загрузки страницы
window.onload = function(){
    setTimeout('Click_Refresh();', 1); //запуск через время
    setInterval('Timer();', 2000); // через 2 сек вызывается метод Присвоить переменной var intervalID;, если захотим остановить
}
var audio = new Audio();
// Проверка совместимости браузеров
function getXmlHttpRequestObject()
{
    if (window.XMLHttpRequest)
    {
        return new XMLHttpRequest(); //To support the browsers IE7+, Firefox, Chrome, Opera, Safari
    }
    else if(window.ActiveXObject)
    {
        return new ActiveXObject("Microsoft.XMLHTTP"); // For the browsers IE6, IE5
    }
    else
    {
        alert("Error due to old verion of browser upgrade your browser");
    }
}
var xmlhttp = new getXmlHttpRequestObject(); //xmlhttp holds the ajax object
// Эта функция вызывается сразу при загрузке или обновлении страницы, прогружая все данные на страницу
function Click_Refresh() {
    if(xmlhttp) {
        xmlhttp.open("GET","Move?action=refresh", true);
        xmlhttp.onreadystatechange = handleServletGet; // Указываем метод, куда придёт ответ от сервлета
        xmlhttp.send(); // собственно запрос
    }
}
// Эта функция вызывается по таймеру, обновляя данные при наличии обновлений
function Timer() {
    if(xmlhttp) {
        xmlhttp.open("GET","Move?action=timer", true);
        xmlhttp.onreadystatechange = handleServletGet; // Указываем метод, куда придёт ответ от сервлета
        xmlhttp.send(); // собственно запрос
    }
}
// Занести слово
function Click_Word() {
    if(xmlhttp) {
        // document.<имя тега form>.<имя поля>.value
        var err = "";
        if (document.getElementById("x").value == "") { err = err + "Не указана координата X"; };
        if (document.getElementById("y").value == "") { err = err + "Не указана координата Y"; };
        if (document.getElementById("word").value == "") { err = err + "Не указано слово"; };
        if (err == "") {
            var parm = "action=word&x=" + document.getElementById("x").value +
                "&y=" + document.getElementById("y").value +
                "&word=" + document.getElementById("word").value +
                "&direction=" + document.radioform.direction.value;
            xmlhttp.open("GET", "Move?" + parm, true);
            xmlhttp.onreadystatechange = handleServletGet; // Указываем метод, куда придёт ответ от сервлета
            xmlhttp.send(); // собственно запрос
            //document.getElementById("word").value = "";
        } else {
            alert(err);
        }
    }
}
// передать ход
function Click_MakeMove() {
    if(xmlhttp) {
        xmlhttp.open("GET","Move?action=makemove", true);
        xmlhttp.onreadystatechange = handleServletGet; // Указываем метод, куда придёт ответ от сервлета
        xmlhttp.send(); // собственно запрос
    }
}
// Обновить буквы с пропуском хода
function Click_RefreshLetters() {
    var parm = "action=refreshletters&count=" + document.getElementById("refreshlettersount").value;
    xmlhttp.open("GET", "Move?" + parm, true);
    xmlhttp.onreadystatechange = handleServletGet; // Указываем метод, куда придёт ответ от сервлета
    xmlhttp.send(); // собственно запрос
}
// Начать игру
function StartGame() {
    xmlhttp.open("GET", "Move?action=startgame", true);
    xmlhttp.onreadystatechange = handleServletGet; // Указываем метод, куда придёт ответ от сервлета
    xmlhttp.send(); // собственно запрос
}

// Сюда придет ответ от сервлета, который в данном методе разбирается и распихивается в нужные места по id
function handleServletGet() {
    if (xmlhttp.readyState == 4) { // Операция полностью завершена
        if(xmlhttp.status == 200) { // Завершена успешно
            //document.getElementById("message").innerHTML = xmlhttp.responseText;
            var s = xmlhttp.responseText;
            var param = s.split('|');
            /* параметр
             0 - action
             1 - gameId
             2 - userId
             3 - userName
             4 - message
             5 - id игрока делающего ход
             6 - список игроков в формате HTML
             7 - доступные буквы
             8 - кол-во оставшихся букв
             9 - возможность заменять буквы с пропуском хода (0 - нет, 1 - да)
             10 - условие окончания игры (0 - не закончена, 1 - закончена
             11 - начата ли игра (0 - начата, 1 - не начата)
             12 - матрица
             */
            if (s.charAt(0) == '=') {
                document.location.href = param[0].substr(1, param[0].length - 1);
            }
            if (param[10] == "1") {
                window.location = "/game-over.jsp";
            }
            if (param.length >= 3) { // Если параметров маловато, то ничего не делаем
                audio.src = 'assets/action.wav';
                audio.autoplay = true;
                document.getElementById("message").innerHTML = param[4];
                document.getElementById("userlist").innerHTML = param[6];
                document.getElementById("username").innerHTML = param[3];
                document.getElementById("letters").innerHTML = param[7];
                document.getElementById("availableletterscount").innerHTML = param[8];
                if (param[11] == "1") {
                    document.getElementById("availablegame").innerHTML = "Игра ещё не началась";
                }  else {
                    document.getElementById("availablegame").innerHTML = "";
                }
                if (param[11] == "0" || param[2] != "0") {
                    document.getElementById("bstartgame").style.display = "none";
                }
                // проверка можно ли нам ходить
                document.getElementById("bword").disabled = (param[2] != param[5] || param[11] == "1");
                document.getElementById("bmakemove").disabled = (param[2] != param[5] || param[9] == "1");
                document.getElementById("brefreshletters").disabled = (param[2] != param[5] || param[9] == "0" || param[11] == "1");
                // не катит с NAME так как это не поле ввода вывода - document.myform.message.value = xmlhttp.responseText;
                var matrix = param[12].split("!"); // 12-й параметр это матрица
                for (var i = 0; i < 225; i++) {
                    document.getElementById("a" + i.toString()).innerHTML = matrix[i].toUpperCase();
                }
            }
        }
        else {
            document.getElementById("message").innerHTML = " - Ошибка соединения";
            alert(xmlhttp.status + " - Ошибка соединения");
        }
    }
}
// Тык-тык в табличку
function setPosition(x,y) {
    document.getElementById("x").value = x;
    document.getElementById("y").value = y;
}