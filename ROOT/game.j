<html>
<head>
  <title>Игра "Эрудит"</title>
    <%@ page contentType="text/html; charset=UTF-8" language="java" %>
  <script type="text/javascript" src="game.js"></script>

  <style type="text/css">
    table,th,td
    {
      border:1px solid black;
      text-align:center;
      vertical-align:center;
    }
    th
    {
      height:30px;
      width:30px;
      vertical-align:center;
    }
    td
    {
      height:30px;
      width:30px;
      vertical-align:center;
    }

  </style>
</head>
<body>
<%String userId = Servlets.Move.getCookieParam(request, "userId");
    String gameId = Servlets.Move.getCookieParam(request, "gameId");
%>
<style type="text/css">
    .stable {font-size:15px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
    .stable th {font-size:15px;background-color:#acc8cc;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:center;}
    .stable tr {background-color:#d4e3e5;}
    .stable td {font-size:15px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:center;}
    .stable td:hover {background-color:#ffffff;}
    .first {font-size:15px;background-color:#acc8cc;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:center;font-weight:bold;}
    .go {background-color:#50c878;}
    .banned {background-color:#f7943c;}
    .block1 {width: 670px;float: left;margin-right: 20px;}
    .block2 {clear: both}
    .block3 {float: left;}
</style>
<div class="block1"><%=Servlets.Move.matrixHTML()%></div>
<div id="availablegame"></div>
<br>
<input id="bstartgame" type="button" value="Начать игру" onclick="StartGame();">
<br><br>
<b>Тебя зовут:</b> <i id="username"></i><br>
<b>Доступные буквы:</b> <i  id="letters"></i><br>
<b>Осталось фишек с буквами:</b> <i  id="availableletterscount"></i><br><br>
<a href="/rules.html" target="_blank">Правила игры</a><br><br>
<div class="block3" id="userlist"></div>
<br>
<div class="block2">
    <br>
    Кол-во изменяемых букв: <input type="text" id="refreshlettersount" size="1" value="0">  <input id="brefreshletters" type="button" value="Обновить буквы и пропустить ход" onclick="Click_RefreshLetters();" >
    <br><br>
    Введите слово: <input type="text" id="word" size="7" maxlength="7"><br>
    Введите координаты начала (x - по горизонтали / y - по вертикали):
    <input type="number" id="x" size="1" min="1" max="15">  <input type="number" id="y" size="1" min="1" max="15"><br>
    <form name="radioform">
    <input type="radio" name="direction" value="v" checked>Вертикально<br>
    <input type="radio" name="direction" value="h">Горизонтально</form>
    </form>
    <input id="bword" type="button" value="Занести слово" onclick="Click_Word();">
    <input id="bmakemove" type="button" value="Сделать ход" onclick="Click_MakeMove();"><br><br>
    Сообщение: <i id="message"></i><br><br>
</div>

</body>
</html>