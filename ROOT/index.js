// событие вызывается после полной загрузки страницы
window.onload = function(){
    setTimeout('CMD_Refresh();', 1); //запуск через время
    timer = setInterval('CMD_Timer();', 5000); // через 5 сек вызывается метод Присвоить переменной var intervalID;, если захотим остановить
}

// Проверка совместимости браузеров
function getXmlHttpRequestObject()
{
    //var xmlHttp = false;
    if (window.XMLHttpRequest)
    {
        return new XMLHttpRequest(); //To support the browsers IE7+, Firefox, Chrome, Opera, Safari
    }
    else if(window.ActiveXObject)
    {
        return new ActiveXObject("Microsoft.XMLHTTP"); // For the browsers IE6, IE5
    }
    else
    {
        alert("Error due to old verion of browser upgrade your browser");
    }
}
var xmlhttp = new getXmlHttpRequestObject(); //xmlhttp holds the ajax object
// Эта функция вызывается сразу при загрузке или обновлении страницы, прогружая все данные на страницу
function CMD_Refresh() {
    if(xmlhttp) {
        xmlhttp.open("GET","EnterGame?action=refresh", true);
        xmlhttp.onreadystatechange = handleServletGet; // Указываем метод, куда придёт ответ от сервлета
        xmlhttp.send(); // собственно запрос
    }
}
// Эта функция вызывается по таймеру
function CMD_Timer() {
    if(xmlhttp) {
        xmlhttp.open("GET","EnterGame?action=timer", true);
        xmlhttp.onreadystatechange = handleServletGet; // Указываем метод, куда придёт ответ от сервлета
        xmlhttp.send(); // собственно запрос
    }
}
function ClickEnterGame() {
    clearTimeout(timer);
    if(xmlhttp) {
        var req = "EnterGame?action=enterGame&username=" + document.getElementById("username").value +
            "&gameId=" + document.getElementById("gameId").value;
        xmlhttp.open("GET", req, true);
        xmlhttp.onreadystatechange = handleServletGet; // Указываем метод, куда придёт ответ от сервлета
        xmlhttp.send(); // собственно запрос
    }
}
// Сюда придет ответ от сервлета
function handleServletGet() {
    if (xmlhttp.readyState == 4) { // Операция полностью завершена
        if(xmlhttp.status == 200) { // Завершена успешно
            var s = xmlhttp.responseText;
            if (s.charAt(0) == '=') {
                document.location.href = s.substr(1, s.length - 1);
            } else {
                document.getElementById("gamelist").innerHTML = xmlhttp.responseText;
            }
        }
        else {
            alert(xmlhttp.status + " - Ошибка соединения");
        }
    }
}