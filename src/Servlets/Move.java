package Servlets;

import NotStaticClasses.Game;
import NotStaticClasses.Player;
import StaticClasses.General;
import StaticClasses.Points;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Rustem Saitgareev, Rustem Khairutdinov, Arslan Tachmuradov
 *         11-602, ITIS, KPFU
 */
@WebServlet(urlPatterns ="/Move")
public class Move extends HttpServlet {
    private static final long serialVersionUID = 1L;


    // Генерация HTML кода матрицы
    public static String matrixHTML() {
        StringBuilder s = new StringBuilder("<TABLE class=\"stable\"><tr><th></th> ");
        for (int x = 1; x <= 15; x++) {
            s.append("<th>");
            s.append(x);
            s.append("</th>");
        }
        s.append("\n");
        int n = 0;
        String color;
        for (int y = 1; y <= 15; y++) {
            s.append("<tr>\n<td class=\"first\">");
            s.append(y);
            s.append("</td>\n");
            for (int x=1; x <= 15; x++) {
                // Буквы
                color = "";
                if (Points.letterCoefficient[y-1].charAt(x-1) ==  '3') {
                    color = " bgcolor=yellow ";
                }
                if (Points.letterCoefficient[y-1].charAt(x-1) ==  '2') {
                    color = " bgcolor=green ";
                }
                // Слова
                if (Points.wordCoefficient[y-1].charAt(x-1) ==  '3') {
                    color = " bgcolor=red ";
                }
                if (Points.wordCoefficient[y-1].charAt(x-1) ==  '2') {
                    color = " bgcolor=cyan ";
                }
                s.append("<TD id=\"a");
                s.append(n);
                s.append("\"");
                s.append(color);
                s.append("onclick=\"setPosition(");
                s.append(x);
                s.append(",");
                s.append(y);
                s.append(");\" ></TD>\n");
                n++;
            }
            s.append("</tr>\n");
        }
        s.append("</TABLE>");
        return s.toString();
    }

    /* формирование строки для response
     формат "ACTION|GAMEID|USERID|USERNAME|MESSAGE|<userId - чей ход>|<имя ходящего игрока>|<список игроков в HTML>|<матрица элемент 1>!<матрица элемент 1>!...!<матрица элемент 235>"
     параметр
           0 - action
           1 - gameId
           2 - userId
           3 - userName
           4 - message
           5 - id игрока делающего ход
           6 - список игроков в формате HTML
           7 - доступные буквы
           8 - кол-во оставшихся букв
           9 - возможность заменять буквы с пропуском хода (0 - нет, 1 - да)
           10 - условие окончания игры (0 - не закончена, 1 - закончена
           11 - начата ли игра (0 - начата, 1 - не начата)
           12 - матрица
    */
    private String responseData(HttpServletRequest request, String action, String message) {
        int gameId = Integer.parseInt(getCookieParam(request, "gameId"));
        int userId = Integer.parseInt(getCookieParam(request, "userId"));
        Game thisGame = General.getGame(gameId);
        Player thisPlayer = thisGame.getPlayer(userId);
        if (General.getGame(gameId).gameOverCondition().equals("1")) {
            General.getGame(gameId).sortPlayers();
        }
        int availableGame = thisGame.isAvailable() ? 1 : 0;
        StringBuilder res = new StringBuilder(action);
        res.append("|");
        res.append(gameId);
        res.append("|");
        res.append(userId);
        res.append("|");
        res.append(getCookieParam(request, "username"));
        res.append("|");
        res.append(message);
        res.append("|");
        res.append(thisGame.getGoId());
        res.append("|");
        res.append(thisGame.userList());
        res.append("|");
        res.append(thisPlayer.getAvailableLetters());
        res.append("|");
        res.append(thisGame.getAllLetters().size());
        res.append("|");
        res.append(thisPlayer.getAbilityToChangeLetters());
        res.append("|");
        res.append(thisGame.gameOverCondition());
        res.append("|");
        res.append(availableGame);
        res.append("|");
        // тут прилепляем матрицу
        for (int i = 0; i < 15; i++) {
            for (int j = 0; j < 15; j++) {
                res.append(thisGame.getMatrix().showLetter(i, j));
                res.append("!");
            }
        }
        return res.toString();
    }

    // получить куки по имени
    public static String getCookieParam(HttpServletRequest request, String name) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                Cookie c = cookies[i];
                if (c.getName().equals(name)) {
                    return c.getValue();
                }
            }
        }
        return "0";
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        String action = request.getParameter("action");
        int userRefreshCounter = Integer.parseInt(getCookieParam(request, "userRefreshCounter"));
        int gameId = Integer.parseInt(getCookieParam(request, "gameId"));
        int userId = Integer.parseInt(getCookieParam(request, "userId"));
        Game thisGame = General.getGame(gameId);
        Player thisPlayer = thisGame.getPlayer(userId);

        if (action.equals("timer")) {
            thisPlayer.setUserTime(System.currentTimeMillis());
            if (userRefreshCounter != thisGame.generalRefreshCounter) {
                out.println(responseData(request, action, "На странице произошли изменения"));
                response.addCookie(new Cookie("userRefreshCounter", Integer.toString(thisGame.generalRefreshCounter)));
            }
        }
        if (action.equals("refresh")) {
            out.println(responseData(request, action, "Страница загружена"));
        }
        if (action.equals("word")) {
            int x = Integer.parseInt(request.getParameter("x"));
            int y = Integer.parseInt(request.getParameter("y"));
            String word = request.getParameter("word");
            String direction = request.getParameter("direction");
            thisGame.getMatrix().setNewMatrix(word, y-1, x-1, direction, gameId, userId);
            int newRefreshCount = ++thisGame.generalRefreshCounter;
            response.addCookie(new Cookie("userRefreshCounter", Integer.toString(newRefreshCount)));
            if (thisGame.getMatrix().getAnswer().equals("Данные получены и успешно обработаны!")) {
                thisPlayer.setAbilityToChangeLetters(0); // После занесения уже не может заменять буквы в течение хода
            }
            out.println(responseData(request, action, thisGame.getMatrix().getAnswer()));
        }
        if (action.equals("makemove")) {
            thisPlayer.setAbilityToChangeLetters(1); // Со следующего хода сможет заменять буквы с пропуском хода
            // Если у пользователя не остаётся букв, то игра завершается (выполнилось одно из условий)
            if (thisPlayer.getAvailableLetters().size() == 0) {
                thisGame.playerHasEndedLetters = true;
            }
            if (thisPlayer.getLackOfLetters() == 7) {
                thisPlayer.setScore(thisPlayer.getScore() + 15);
            }
            thisGame.getRandomLetters(thisPlayer.getAvailableLetters(), thisPlayer.getLackOfLetters());
            do {
                if (thisGame.getGoId() == thisGame.getLastUserId()) {
                    thisGame.setGoId(0);
                } else {
                    thisGame.setGoId(thisGame.getGoId() + 1);
                }
            } while (thisGame.getPlayer(thisGame.getGoId()).isBanned());
            int newRefreshCount = ++thisGame.generalRefreshCounter;
            response.addCookie(new Cookie("userRefreshCounter", Integer.toString(newRefreshCount)));
            out.println(responseData(request, action, "Ход передан"));
        }
        if (action.equals("refreshletters")) {
            thisGame.missedMovesCount++; // Прибавляем число пропущенных ходов (условие завершения)
            int count = Integer.parseInt(request.getParameter("count"));
            thisGame.refreshRandomLetters(thisPlayer.getAvailableLetters(), count);
            do {
                if (thisGame.getGoId() == thisGame.getLastUserId()) {
                    thisGame.setGoId(0);
                } else {
                    thisGame.setGoId(thisGame.getGoId() + 1);
                }
            } while (thisGame.getPlayer(thisGame.getGoId()).isBanned());
            System.out.println(thisGame.getGoId());
            int newRefreshCount = ++thisGame.generalRefreshCounter;
            response.addCookie(new Cookie("userRefreshCounter", Integer.toString(newRefreshCount)));
            out.println(responseData(request, action, "Буквы заменены, ход передан"));
        }
        if (action.equals("startgame")) {
            thisGame.setAvailable(false);
            int newRefreshCount = ++thisGame.generalRefreshCounter;
            response.addCookie(new Cookie("userRefreshCounter", Integer.toString(newRefreshCount)));
            out.println(responseData(request, action, "Игра началась!"));
        }
    }
}