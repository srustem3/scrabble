package Servlets;

import StaticClasses.General;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * @author Rustem Saitgareev, Rustem Khairutdinov, Arslan Tachmuradov
 *         11-602, ITIS, KPFU
 */
@WebServlet(name="EnterGame", urlPatterns={"/EnterGame"})
public class EnterGame extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        String action = request.getParameter("action");
        if (action.equals("refresh")) {
            out.println(General.gameList());
        }
        if (action.equals("timer")) {
            out.println(General.gameList());
        }
        if (action.equals("enterGame")) {
            String username = request.getParameter("username");
            if (username.equals("ResetAllNow")) { // Чит-код для очистки всех игр
                General.setGames(new ArrayList<>());
                out.println("=/");
            } else if (username.equals("ResetSingleNow")) { // Чит-код для установки доступности конкретной игры
                int gameId = Integer.parseInt(request.getParameter("gameId"));
                gameId--;
                General.getGame(gameId).setAvailable(false);
                out.println("=/");
            } else {
                int userId;
                int gameId = Integer.parseInt(request.getParameter("gameId"));
                if (gameId == 0) {
                    gameId = General.addGame(username);
                } else {
                    gameId--; // Нужно убавить, т.к. на странице человек видит с 1, а не с 0. 0 для него - это создание новой игры
                }
                if (gameId < 0) {
                    out.println("=game-not-found-error.jsp");
                } else if (gameId <= General.getLastGameId() && General.getGame(gameId).isAvailable()) {
                    General.getGame(gameId).generalRefreshCounter++;
                    userId = General.getGame(gameId).addPlayer(username);
                    General.getGame(gameId).getRandomLetters(General.getGame(gameId).getPlayer(userId).getAvailableLetters(), 7);
                    response.addCookie(new Cookie("username", username));
                    response.addCookie(new Cookie("gameId", Integer.toString(gameId)));
                    response.addCookie(new Cookie("userId", Integer.toString(userId)));
                    response.addCookie(new Cookie("userRefreshCounter", Integer.toString(General.getGame(gameId).generalRefreshCounter)));
                    out.println("=game.jsp");
                } else {
                    out.println("=game-not-found-error.jsp");
                }
            }
        }
    }
}
