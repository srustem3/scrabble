package StaticClasses;

import java.sql.*;
import java.util.Properties;

/**
 * @author Rustem Saitgareev, Rustem Khairutdinov, Arslan Tachmuradov
 *         11-602, ITIS, KPFU
 */
public class DataBase {
    // JDBC URL of MySQL server
    private static final String url = "jdbc:mysql://localhost:3306/scrabble?useSSL=false";

    // JDBC variables for opening and managing connection
    private static Connection con;
    public static Statement stmt;

    public static void connectionToDB() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            // Параметры соединения с базой
            Properties connInfo = new Properties();
            connInfo.put("user","root");
            connInfo.put("password","root");
            connInfo.put("useUnicode","true");
            connInfo.put("characterEncoding","KOI8_R");
            con = DriverManager.getConnection(url,connInfo);
            stmt = con.createStatement();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            try { con.close(); } catch(SQLException e) {
                System.out.println(e.getMessage());
            }
            try { stmt.close(); } catch(SQLException e) {
                System.out.println(e.getMessage());
            }
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void disconnectionFromDB() {
        try { con.close(); } catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
        try { stmt.close(); } catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static Statement getStmt() {
        return stmt;
    }
}
