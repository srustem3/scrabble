package StaticClasses;

import NotStaticClasses.Game;

import java.util.ArrayList;

/**
 * @author Rustem Saitgareev, Rustem Khairutdinov, Arslan Tachmuradov
 *         11-602, ITIS, KPFU
 */
public class General {
    private static ArrayList<Game> games = new ArrayList<>();

    public static Game getGame(int number) {
        return games.get(number);
    }

    public static void setGames(ArrayList<Game> games) {
        General.games = games;
    }

    public static String gameList() {
        StringBuilder result = new StringBuilder("<table class=\"stable\" border=\"1\">\n" +
                "<tr><th>№</th><th>Создатель комнаты</th><th>Кол-во подключившихся игроков</th></tr>");
        result.append("<tr><td>0</td><td>Новая игра</td><td>Создай - будешь первым</td>\n");
        for (int id = 0; id < games.size(); id++) {
            Game thisGame = games.get(id);
            if (thisGame.isAvailable()) {
                result.append("<tr><td>" + (id + 1) + "</td><td>" + thisGame.getGameName() + "</td><td>" + Integer.toString(thisGame.getLastUserId() + 1) + "</td>\n");
            }
        }
        result.append("</table>");
        return result.toString();
    }

    public static int addGame(String userName) {
        games.add(new Game(userName));
        return games.size() - 1;
    }

    public static int getLastGameId() {
        return games.size() - 1;
    }
}
