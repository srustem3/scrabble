package StaticClasses;

import java.util.HashMap;

/**
 * @author Rustem Saitgareev, Rustem Khairutdinov, Arslan Tachmuradov
 *         11-602, ITIS, KPFU
 */
public class Points {
    // Ассоциативный список для хранения очков каждой буквы
    public static HashMap<Character, Integer> scoreForLetters = new HashMap<>();
    // Коэффициенты букв
    public static final String[] letterCoefficient =
            {"111211111112111",
                    "111113111311111",
                    "111111212111111",
                    "211111121111112",
                    "111111111111111",
                    "131111111111131",
                    "112111212111211",
                    "111211111112111",
                    "112111212111211",
                    "131111111111131",
                    "111111111111111",
                    "211111121111112",
                    "111111212111111",
                    "111113111311111",
                    "311211131112113"};
    // Коэффициенты слов
    public static final String[] wordCoefficient =
            {"311111131111113",
                    "121111111111121",
                    "112111111111211",
                    "111211111112111",
                    "111121111121111",
                    "111111111111111",
                    "111111111111111",
                    "311111111111113",
                    "111111111111111",
                    "111111111111111",
                    "111121111121111",
                    "111211111112111",
                    "112111111111211",
                    "121111111111121",
                    "311111131111113"};

    static {
        scoreForLetters.put('а', 1);
        scoreForLetters.put('б', 3);
        scoreForLetters.put('в', 2);
        scoreForLetters.put('г', 3);
        scoreForLetters.put('д', 2);
        scoreForLetters.put('е', 1);
        scoreForLetters.put('ж', 5);
        scoreForLetters.put('з', 5);
        scoreForLetters.put('и', 1);
        scoreForLetters.put('й', 2);
        scoreForLetters.put('к', 2);
        scoreForLetters.put('л', 2);
        scoreForLetters.put('м', 2);
        scoreForLetters.put('н', 1);
        scoreForLetters.put('о', 1);
        scoreForLetters.put('п', 2);
        scoreForLetters.put('р', 2);
        scoreForLetters.put('с', 2);
        scoreForLetters.put('т', 2);
        scoreForLetters.put('у', 3);
        scoreForLetters.put('ф', 10);
        scoreForLetters.put('х', 5);
        scoreForLetters.put('ц', 10);
        scoreForLetters.put('ч', 5);
        scoreForLetters.put('ш', 10);
        scoreForLetters.put('щ', 10);
        scoreForLetters.put('ъ', 10);
        scoreForLetters.put('ы', 5);
        scoreForLetters.put('ь', 5);
        scoreForLetters.put('э', 10);
        scoreForLetters.put('ю', 10);
        scoreForLetters.put('я', 3);
    }
}
