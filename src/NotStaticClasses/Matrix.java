package NotStaticClasses;

import StaticClasses.DataBase;
import StaticClasses.General;

import java.sql.*;
import java.util.ArrayList;

/**
 * @author Rustem Saitgareev, Rustem Khairutdinov, Arslan Tachmuradov
 *         11-602, ITIS, KPFU
 */
public class Matrix {
    private Statement st;
    private char[][] workMatrix = new char[15][15]; // Рабочая матрица, готовая для отправки в базу. В ней всегда всё хорошо
    private char[][] tempMatrix = new char[15][15]; // Временная матрица для проверки существования слов
    private String answer = ""; // Ответ для вывода на экран пользователю
    private String unfoundWord = ""; // Если слово не найдено в базе, оно присваивается этой переменной и выводится

    public Matrix() {
        for (int i = 0; i < 15; i++) {
            for (int j = 0; j < 15; j++) {
                workMatrix[i][j] = ' ';
            }
        }
    }

    public String showLetter(int i, int j) {
        return Character.toString(workMatrix[i][j]);
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public void setSt(Statement statement) {
        st = statement;
    }

    // Позволяет вывести в таблицу конкретный элемент матрицы
    public char returnElement(int i, int j) {
        return workMatrix[i][j];
    }

    // Проверка и запись обновлённой матрицы после хода игрока
    public void setNewMatrix(String word, int i, int j, String direction, int gameId, int userId) {
        String allowedLetters = General.getGame(gameId).getPlayer(userId).getAvailableLettersAsString();
        word = word.toLowerCase();
        try {
            // Проверка выхода за границу
            if (direction.equals("v") && i + word.length() > 15) {
                throw new Exception("Слово вышло за границы");
            }
            if (direction.equals("h") && j + word.length() > 15) {
                throw new Exception("Слово вышло за границы");
            }
            if (!checkConnection(word, j, i, direction.equals("v"))) {
                throw new Exception("Слово должно касаться другого или проходить через него (первый ход через центр)");
            }
            DataBase.connectionToDB();
            st = DataBase.getStmt();
            if (!checkMatrix(word, i, j, direction)) {
                throw new Exception("Найдено несуществующее слово: " + unfoundWord);
            }
            DataBase.disconnectionFromDB();
            // Сюда программа дойдёт только тогда, когда почти все проверки пройдены. Выявляем, какие буквы куда были занесены и начисляем очки
            ArrayList<Character> keep = new ArrayList<>(General.getGame(gameId).getPlayer(userId).getAvailableLetters());
            for (int k = 0; k < workMatrix.length; k++) {
                for (int l = 0; l < workMatrix.length; l++) {
                    if (workMatrix[k][l] != tempMatrix[k][l]) {
                        if (!General.getGame(gameId).getPlayer(userId).removeLetter(tempMatrix[k][l])) {
                            if (!General.getGame(gameId).getPlayer(userId).removeLetter('*')) {
                                General.getGame(gameId).getPlayer(userId).setAvailableLetters(keep);
                                throw new Exception("Использованы недопустимые буквы");
                            }
                        }
                        General.getGame(gameId).getPlayer(userId).scoreAddition(k, l, tempMatrix[k][l]);
                    }
                }
            }
            General.getGame(gameId).getPlayer(userId).wordCoefficientMultiplication();
            // Заполняем матрицу
            for (int k = 0; k < workMatrix.length; k++) {
                for (int l = 0; l < workMatrix.length; l++) {
                    workMatrix[k][l] = tempMatrix[k][l];
                }
            }
            answer = "Данные получены и успешно обработаны!";
        } catch (Exception ex) {
            answer = ex.getMessage();
        }
    }
    // Далее идут методы проверки матрицы, которые использованы выше
    // ===========================================================================================================
    public boolean checkMatrix(String word, int i, int j, String direction) {
        for (int k = 0; k < tempMatrix.length; k++) {
            for (int l = 0; l < tempMatrix.length; l++) {
                tempMatrix[k][l] = workMatrix[k][l];
            }
        }
        for (int k = 0; k < word.length(); k++) {
            if (direction.equals("v")) {
                tempMatrix[i + k][j]  = word.charAt(k);
            }
            if (direction.equals("h")) {
                tempMatrix[i][j + k]  = word.charAt(k);
            }
        }
        return checkHorizontal() && checkVertical();
    }

    private boolean checkHorizontal() {
        for (int i = 0; i < tempMatrix.length; i++) {
            String word = "";

            for (int j = 0; j < tempMatrix.length; j++) {
                word += tempMatrix[i][j];
            }

            String[] words = word.split(" ");

            for (int k = 0; k < words.length; k++) {
                if (words[k].length() > 1 && !checkWord(words[k])) return false;
            }
        }
        return true;
    }

    private boolean checkVertical() {
        for (int i = 0; i < tempMatrix.length; i++) {
            String word = "";

            for (int j = 0; j < tempMatrix.length; j++) {
                word += tempMatrix[j][i];
            }

            String[] words = word.split(" ");

            for (int k = 0; k < words.length; k++) {
                if (words[k].length() > 1 && !checkWord(words[k])) return false;
            }
        }
        return true;
    }

    public boolean checkWord(String word) {
        ResultSet rs = null;
        try {
            rs = st.executeQuery("select count(*) from dictionary where word = '" + word + "'");
            while (rs.next()) {
                if (rs.getInt(1) > 0) {
                    return true;
                } else {
                    unfoundWord = word;
                    return false;
                }
            }
        } catch (SQLException ex) {
            answer = ex.getMessage();
        } finally {
            try {
                rs.close();
            } catch (Exception ex) {
                answer = ex.getMessage();
            }
        }
        return false;
    }
    // =============================================================================================================
    private boolean isFirstTurn() {
        for (int i = 0; i < 15; i++) {
            for (int j = 0; j < 15; j++) {
                if (workMatrix[i][j] != ' ') {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean checkConnection(String word,int x,int y,boolean vertical){
        boolean check = false;
        if(isFirstTurn()){
            if(checkConnectionFirst(word,x,y,vertical)) {
                check = true;
            }
        }else{
            if(checkConnectionSecond(word,x,y,vertical)){
                check = true;
            }
        }
        return check;
    }
    // checkConnectionFirst - метод для проверки первого хода,проходит ли слово по центру
    private boolean checkConnectionFirst(String word,int x,int y,boolean vertical){
        boolean check = false;
        if(vertical){
            for(int i = y;(i < y + word.length())&& !check;i++){
                if((i == 7) && (x == 7)){
                    check = true;
                }
            }
        }else{
            for(int i = x;(i < x + word.length()) && !check;i++){
                if((i == 7) && (y == 7)){
                    check = true;
                }
            }
        }
        return check;
    }
    // checkConnectionSecond - метод для проверки НЕ первого хода,проходит ли слово через другие слова
    private boolean checkConnectionSecond(String word,int x,int y,boolean vertical){
        boolean check = false;
        int countWord = 0;
        int countTable = 0;
        if(vertical){
            for(int i = y;i < y + word.length();i++){
                if((workMatrix[i][x] >= 'a') && (workMatrix[i][x] <= 'я')){
                    countTable++;
                    if(workMatrix[i][x] == word.charAt(i-y)){
                        countWord++;
                    }
                }
            }
        }else{
            for(int i = x;i < x + word.length();i++) {
                if ((workMatrix[y][i] >= 'a') && (workMatrix[y][i] <= 'я')) {
                    countTable++;
                    if (workMatrix[y][i] == word.charAt(i - x)) {
                        countWord++;
                    }
                }
            }
        }
        boolean checkLen = (countWord != word.length());
        boolean checkCharacters = (countWord == countTable);
        boolean checkZero = (countWord != 0);
        if(!checkCharacters){
            check = false;
        }else if(checkLen && checkZero){
            check = true;
        }else{
            check = checkWordsInTable(word, x, y, vertical);
        }
        return check;
    }
    // checkWordsInTable - проверяет на соприкосновение,являются ли все новые слова от соприкосновения реальными словами
    private boolean checkWordsInTable(String word,int x,int y,boolean vertical){
        boolean check = false;
        if(vertical) {
            for (int i = y; i < y + word.length(); i++) {
                if (i == y){
                    if((workMatrix[i - 1][x] >= 'а') && (workMatrix[i - 1][x] <= 'я')){
                        check = true;
                    }
                }
                if(((workMatrix[i][x - 1] >= 'а') && (workMatrix[i][x - 1] <= 'я')) || ((workMatrix[i][x + 1] >= 'а') && (workMatrix[i][x + 1] <= 'я'))){
                    check = true;
                }
                if(i == y + word.length() - 1){
                    if((workMatrix[i + 1][x] >= 'а') && (workMatrix[i + 1][x] <= 'я')){
                        check = true;
                    }
                }
            }
        }else{
            for (int i = x; i < x + word.length(); i++) {
                if (i == x){
                    if((workMatrix[y][i - 1] >= 'а') && (workMatrix[y][i - 1] <= 'я')){
                        check = true;
                    }
                }
                if(((workMatrix[y - 1][i] >= 'а') && (workMatrix[y - 1][i] <= 'я')) || ((workMatrix[y + 1][i] >= 'а') && (workMatrix[y + 1][i] <= 'я'))){
                    check = true;
                }
                if(i == x + word.length() - 1){
                    if((workMatrix[y][i + 1] >= 'а') && (workMatrix[y][i + 1] <= 'я')){
                        check = true;
                    }
                }
            }
        }

        return check;
    }
    //==================================================================================================================
    public void cleanTable() {
        StringBuilder lineToSend = new StringBuilder("");
        for (int i = 0; i < 15; i++) {
            for (int j = 0; j < 15; j++) {
                workMatrix[i][j] = ' '; // Опустошаем матрицу в классе
                lineToSend.append(workMatrix[i][j]); // Добавляем в строчку для отправки в БД
            }
        }
    }
}
