package NotStaticClasses;

import java.util.*;

/**
 * @author Rustem Saitgareev, Rustem Khairutdinov, Arslan Tachmuradov
 *         11-602, ITIS, KPFU
 */
public class Game {
    private String gameName;
    private boolean available = true; // Доступность игры с главной страницы
    private Character[] array = {'а', 'а', 'а', 'а', 'а', 'а', 'а', 'а', 'а', 'а', 'б', 'б', 'б', 'в', 'в', 'в', 'в', 'в', 'г',
            'г', 'г', 'д', 'д', 'д', 'д', 'д', 'е', 'е', 'е', 'е', 'е', 'е', 'е', 'е', 'е', 'ж', 'ж', 'з', 'з', 'и',
            'и', 'и', 'и', 'и', 'и', 'и', 'и', 'й', 'й', 'й', 'й', 'к', 'к', 'к', 'к', 'к', 'к', 'л', 'л', 'л', 'л',
            'м', 'м', 'м', 'м', 'м', 'н', 'н', 'н', 'н', 'н', 'н', 'н', 'н', 'о', 'о', 'о', 'о', 'о', 'о', 'о', 'о',
            'о', 'о', 'п', 'п', 'п', 'п', 'п', 'п', 'р', 'р', 'р', 'р', 'р', 'р', 'с', 'с', 'с', 'с', 'с', 'с', 'т',
            'т', 'т', 'т', 'т', 'у', 'у', 'у', 'ф', 'х', 'х', 'ц', 'ч', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ы', 'ь', 'ь', 'э',
            'ю', 'я', 'я', 'я', '*', '*', '*'};
    private ArrayList<Character> allLetters = new ArrayList<>(Arrays.asList(array));
    private ArrayList<Player> players = new ArrayList<>();
    private int goId = 0;
    private Matrix matrix = new Matrix();
    public int generalRefreshCounter = 0; // Общая переменная-счётчик действий для конкретной игры
    public int missedMovesCount = 0;
    public boolean playerHasEndedLetters = false; // Одно из условий завершения игры, когда у кого-то кончаются все буквы без возможности добора
    private Timer timer = new Timer();

    public Game(String gameName) {
        this.gameName = gameName;
        timer.schedule(new checkUsers(), 20000, 20000);
    }

    private class checkUsers extends TimerTask {
        @Override
        public void run() {
            int count = 0;
            for (int i = 0; i < players.size(); i++) {
                if (players.get(i).getUserTime() + 15000 < System.currentTimeMillis()) {
                    if (!players.get(i).isBanned()) {
                        generalRefreshCounter++;
                    }
                    players.get(i).setBanned(true);
                    count++;
                } else {
                    if (players.get(i).isBanned()) {
                        generalRefreshCounter++;
                    }
                    players.get(i).setBanned(false);
                }
            }
            if (players.get(goId).isBanned()) {
                if (goId == getLastUserId()) {
                    goId = 0;
                } else {
                    goId++;
                }
                generalRefreshCounter++;
            }
            if (count == players.size() || available && players.get(0).isBanned()) {
                available = false;
                playerHasEndedLetters = true; // Заставляем игру завершиться
                generalRefreshCounter++;
            }
            if (playerHasEndedLetters) {
                timer.cancel(); // При завершении останавливаем таймер
            }
        }
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public ArrayList<Character> getAllLetters() {
        return allLetters;
    }

    public Matrix getMatrix() {
        return matrix;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGoId(int goId) {
        this.goId = goId;
    }

    public int getGoId() {
        return goId;
    }

    public int getLastUserId() {
        return players.size() - 1;
    }

    public int addPlayer(String userName) {
        players.add(new Player(userName));
        if (players.size() >= 4) {
            available = false;
        }
        return players.size() - 1;
    }

    public Player getPlayer(int number) {
        return players.get(number);
    }


    public String userList() {
        StringBuilder result = new StringBuilder("<table class=\"stable\" border=\"1\">\n" +
                "<tr><th>№</th><th>Ник</th><th>Очки</th><th>Доступные буквы</th></tr>");
        for (int id = 0; id < players.size(); id++) {
            Player thisPlayer = players.get(id);
            String go = (id == goId) ? "class=\"go\"" : "";
            String banned = thisPlayer.isBanned() ? "class=\"banned\"" : "";
            if (go.equals("class=\"go\"") && banned.equals("class=\"banned\"")) {
                go = "";
            }
            result.append("<tr><td " + go + banned + ">" + (id + 1) + "</td><td " + go + banned + ">" + thisPlayer.getUserName() + "</td><td " + go + banned + ">" + thisPlayer.getScore() + "</td><td " + go + banned + ">" + thisPlayer.getAvailableLetters() + "</td>\n");
        }
        result.append("</table>");
        return result.toString();
    }

    public void sortPlayers() {
        Collections.sort(players);
    }

    public void getRandomLetters(ArrayList<Character> userLetters, int lackOfLetters) {
        for (int i = 0; i < lackOfLetters; i++) {
            if (allLetters.size() > 0) {
                userLetters.add(allLetters.remove((int) (Math.random() * allLetters.size())));
            }
        }
    }

    public void refreshRandomLetters(ArrayList<Character> userLetters, int count) {
        for (int i = 0; i < count; i++) {
            Character removedLetter = userLetters.remove((int)((Math.random() * userLetters.size())));
            allLetters.add(removedLetter);
            getRandomLetters(userLetters, 1);
        }
    }

    public String gameOverCondition() {
        if (playerHasEndedLetters || missedMovesCount == players.size() * 2) {
            playerHasEndedLetters = true;
            return "1";
        }
        return "0";
    }
}
