package NotStaticClasses;

import StaticClasses.Points;

import java.util.ArrayList;

/**
 * @author Rustem Saitgareev, Rustem Khairutdinov, Arslan Tachmuradov
 *         11-602, ITIS, KPFU
 */
public class Player implements Comparable<Player> {
    private String userName;
    private int score = 0; // Общее число очков
    private int scoreForThisMove = 0; // Число очков за текущий ход
    private int wordCoefficient = 1; // Коэффициент за целое слово
    private ArrayList<Character> availableLetters = new ArrayList<>(7);
    private int abilityToChangeLetters = 1;
    private long userTime = System.currentTimeMillis();
    private boolean banned = false;

    public Player(String userName) {
        this.userName = userName;
    }

    public void setUserTime(long userTime) {
        this.userTime = userTime;
    }

    public long getUserTime() {
        return userTime;
    }

    public void setBanned(boolean banned) {
        this.banned = banned;
    }

    public boolean isBanned() {
        return banned;
    }

    public String getUserName() {
        return userName;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getAvailableLettersAsString() {
        StringBuilder res = new StringBuilder("");
        for (int i = 0; i < availableLetters.size(); i++) {
            res.append(availableLetters.get(i));
        }
        return res.toString();
    }

    public ArrayList<Character> getAvailableLetters() {
        return availableLetters;
    }

    public void setAvailableLetters(ArrayList<Character> availableLetters) {
        this.availableLetters = availableLetters;
    }

    public int getAbilityToChangeLetters() {
        return abilityToChangeLetters;
    }

    public void setAbilityToChangeLetters(int abilityToChangeLetters) {
        this.abilityToChangeLetters = abilityToChangeLetters;
    }

    public boolean removeLetter(char letter) {
        if (availableLetters.contains(letter)) {
            availableLetters.remove((Character)letter);
            return true;
        } else if (availableLetters.contains('*')) {
            availableLetters.remove((Character)'*');
            return true;
        } else {
            scoreForThisMove = 0;
            wordCoefficient = 1;
            return false;
        }
    }

    public void scoreAddition(int i, int j, char letter) {
        int letterScore = Points.scoreForLetters.get(letter);
        letterScore *= Character.getNumericValue(Points.letterCoefficient[i].charAt(j));
        wordCoefficient *= Character.getNumericValue(Points.wordCoefficient[i].charAt(j));
        scoreForThisMove += letterScore;
    }

    public void wordCoefficientMultiplication() {
        scoreForThisMove *= wordCoefficient;
        score += scoreForThisMove;
        scoreForThisMove = 0;
        wordCoefficient = 1;
    }

    public int getLackOfLetters() {
        return 7 - availableLetters.size();
    }

    @Override
    public int compareTo(Player second) {
        return second.getScore() - score;
    }
}
